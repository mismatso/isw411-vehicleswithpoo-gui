﻿namespace VehiclesWithPOO
{
    partial class MainForm
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.miMainMenu = new System.Windows.Forms.MenuStrip();
            this.tsmiVehicles = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiCalculateMandatoryInsurance = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.miMainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // miMainMenu
            // 
            this.miMainMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.miMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiVehicles,
            this.tsmiExit});
            this.miMainMenu.Location = new System.Drawing.Point(0, 0);
            this.miMainMenu.Name = "miMainMenu";
            this.miMainMenu.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.miMainMenu.Size = new System.Drawing.Size(1189, 28);
            this.miMainMenu.TabIndex = 0;
            this.miMainMenu.Text = "menuStrip1";
            // 
            // tsmiVehicles
            // 
            this.tsmiVehicles.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiCalculateMandatoryInsurance});
            this.tsmiVehicles.Name = "tsmiVehicles";
            this.tsmiVehicles.Size = new System.Drawing.Size(79, 24);
            this.tsmiVehicles.Text = "Vehículos";
            // 
            // tsmiCalculateMandatoryInsurance
            // 
            this.tsmiCalculateMandatoryInsurance.Name = "tsmiCalculateMandatoryInsurance";
            this.tsmiCalculateMandatoryInsurance.Size = new System.Drawing.Size(249, 26);
            this.tsmiCalculateMandatoryInsurance.Text = "Calcular seguro obligatorio";
            this.tsmiCalculateMandatoryInsurance.Click += new System.EventHandler(this.tsmiCalculateMandatoryInsurance_Click);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(45, 24);
            this.tsmiExit.Text = "Salir";
            this.tsmiExit.Click += new System.EventHandler(this.tsmiExit_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1189, 705);
            this.Controls.Add(this.miMainMenu);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.miMainMenu;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainForm";
            this.Text = "Cálculo de Seguro Obligatorio";
            this.miMainMenu.ResumeLayout(false);
            this.miMainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip miMainMenu;
        private System.Windows.Forms.ToolStripMenuItem tsmiVehicles;
        private System.Windows.Forms.ToolStripMenuItem tsmiCalculateMandatoryInsurance;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
    }
}

