﻿namespace VehiclesWithPOO.Views
{
    partial class MandatoryInsuranceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbFormTitle = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.gbVehicle = new System.Windows.Forms.GroupBox();
            this.btClearForm = new System.Windows.Forms.Button();
            this.btCalculateMandatoryInsurance = new System.Windows.Forms.Button();
            this.gbTruck = new System.Windows.Forms.GroupBox();
            this.lbTypeOfTruck = new System.Windows.Forms.Label();
            this.cbTypeOfTruck = new System.Windows.Forms.ComboBox();
            this.tbNumberOfAxles = new System.Windows.Forms.TextBox();
            this.lbNumberOfAxles = new System.Windows.Forms.Label();
            this.gbBus = new System.Windows.Forms.GroupBox();
            this.lbTypeOfService = new System.Windows.Forms.Label();
            this.cbTypeOfService = new System.Windows.Forms.ComboBox();
            this.tbSeatingCapacity = new System.Windows.Forms.TextBox();
            this.lbSeatingCapacity = new System.Windows.Forms.Label();
            this.gbCar = new System.Windows.Forms.GroupBox();
            this.lbTypeOfAutomobile = new System.Windows.Forms.Label();
            this.cbTypeOfAutomobile = new System.Windows.Forms.ComboBox();
            this.tbNumberOfDoors = new System.Windows.Forms.TextBox();
            this.lbNumberOfDoors = new System.Windows.Forms.Label();
            this.cbColorOfVehicle = new System.Windows.Forms.ComboBox();
            this.cbBrandOfVehicle = new System.Windows.Forms.ComboBox();
            this.tbPassengerCapacity = new System.Windows.Forms.TextBox();
            this.lbFiscalValue = new System.Windows.Forms.Label();
            this.lbPassengerCapacity = new System.Windows.Forms.Label();
            this.lbColor = new System.Windows.Forms.Label();
            this.lbBrand = new System.Windows.Forms.Label();
            this.lbModel = new System.Windows.Forms.Label();
            this.lbRegistration = new System.Windows.Forms.Label();
            this.lbTypeOfVehicle = new System.Windows.Forms.Label();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.tbRegistration = new System.Windows.Forms.TextBox();
            this.tbFiscalValue = new System.Windows.Forms.TextBox();
            this.cbTypeOfVehicle = new System.Windows.Forms.ComboBox();
            this.gbResult = new System.Windows.Forms.GroupBox();
            this.txtMandatoryInsurance = new System.Windows.Forms.TextBox();
            this.lbMandatoryInsurance = new System.Windows.Forms.Label();
            this.panelHeader.SuspendLayout();
            this.gbVehicle.SuspendLayout();
            this.gbTruck.SuspendLayout();
            this.gbBus.SuspendLayout();
            this.gbCar.SuspendLayout();
            this.gbResult.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbFormTitle
            // 
            this.lbFormTitle.AutoSize = true;
            this.lbFormTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFormTitle.Location = new System.Drawing.Point(16, 11);
            this.lbFormTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbFormTitle.Name = "lbFormTitle";
            this.lbFormTitle.Size = new System.Drawing.Size(691, 52);
            this.lbFormTitle.TabIndex = 0;
            this.lbFormTitle.Text = "Calculadora de Seguro Obligatorio";
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelHeader.Controls.Add(this.lbFormTitle);
            this.panelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeader.Location = new System.Drawing.Point(0, 0);
            this.panelHeader.Margin = new System.Windows.Forms.Padding(4);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(923, 73);
            this.panelHeader.TabIndex = 0;
            // 
            // gbVehicle
            // 
            this.gbVehicle.Controls.Add(this.btClearForm);
            this.gbVehicle.Controls.Add(this.btCalculateMandatoryInsurance);
            this.gbVehicle.Controls.Add(this.gbTruck);
            this.gbVehicle.Controls.Add(this.gbBus);
            this.gbVehicle.Controls.Add(this.gbCar);
            this.gbVehicle.Controls.Add(this.cbColorOfVehicle);
            this.gbVehicle.Controls.Add(this.cbBrandOfVehicle);
            this.gbVehicle.Controls.Add(this.tbPassengerCapacity);
            this.gbVehicle.Controls.Add(this.lbFiscalValue);
            this.gbVehicle.Controls.Add(this.lbPassengerCapacity);
            this.gbVehicle.Controls.Add(this.lbColor);
            this.gbVehicle.Controls.Add(this.lbBrand);
            this.gbVehicle.Controls.Add(this.lbModel);
            this.gbVehicle.Controls.Add(this.lbRegistration);
            this.gbVehicle.Controls.Add(this.lbTypeOfVehicle);
            this.gbVehicle.Controls.Add(this.tbModel);
            this.gbVehicle.Controls.Add(this.tbRegistration);
            this.gbVehicle.Controls.Add(this.tbFiscalValue);
            this.gbVehicle.Controls.Add(this.cbTypeOfVehicle);
            this.gbVehicle.Location = new System.Drawing.Point(16, 80);
            this.gbVehicle.Margin = new System.Windows.Forms.Padding(4);
            this.gbVehicle.Name = "gbVehicle";
            this.gbVehicle.Padding = new System.Windows.Forms.Padding(4);
            this.gbVehicle.Size = new System.Drawing.Size(891, 356);
            this.gbVehicle.TabIndex = 1;
            this.gbVehicle.TabStop = false;
            this.gbVehicle.Text = "Datos del Vehículo";
            // 
            // btClearForm
            // 
            this.btClearForm.Image = global::VehiclesWithPOO.Properties.Resources.edit_clear_3;
            this.btClearForm.Location = new System.Drawing.Point(233, 268);
            this.btClearForm.Margin = new System.Windows.Forms.Padding(4);
            this.btClearForm.Name = "btClearForm";
            this.btClearForm.Size = new System.Drawing.Size(193, 68);
            this.btClearForm.TabIndex = 18;
            this.btClearForm.Text = "Limpiar";
            this.btClearForm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btClearForm.UseVisualStyleBackColor = true;
            // 
            // btCalculateMandatoryInsurance
            // 
            this.btCalculateMandatoryInsurance.Image = global::VehiclesWithPOO.Properties.Resources.dialog_ok_apply_2;
            this.btCalculateMandatoryInsurance.Location = new System.Drawing.Point(29, 268);
            this.btCalculateMandatoryInsurance.Margin = new System.Windows.Forms.Padding(4);
            this.btCalculateMandatoryInsurance.Name = "btCalculateMandatoryInsurance";
            this.btCalculateMandatoryInsurance.Size = new System.Drawing.Size(193, 68);
            this.btCalculateMandatoryInsurance.TabIndex = 17;
            this.btCalculateMandatoryInsurance.Text = "Calcular";
            this.btCalculateMandatoryInsurance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btCalculateMandatoryInsurance.UseVisualStyleBackColor = true;
            // 
            // gbTruck
            // 
            this.gbTruck.Controls.Add(this.lbTypeOfTruck);
            this.gbTruck.Controls.Add(this.cbTypeOfTruck);
            this.gbTruck.Controls.Add(this.tbNumberOfAxles);
            this.gbTruck.Controls.Add(this.lbNumberOfAxles);
            this.gbTruck.Location = new System.Drawing.Point(455, 238);
            this.gbTruck.Margin = new System.Windows.Forms.Padding(4);
            this.gbTruck.Name = "gbTruck";
            this.gbTruck.Padding = new System.Windows.Forms.Padding(4);
            this.gbTruck.Size = new System.Drawing.Size(407, 98);
            this.gbTruck.TabIndex = 16;
            this.gbTruck.TabStop = false;
            this.gbTruck.Text = "Camión";
            // 
            // lbTypeOfTruck
            // 
            this.lbTypeOfTruck.AutoSize = true;
            this.lbTypeOfTruck.Location = new System.Drawing.Point(23, 60);
            this.lbTypeOfTruck.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTypeOfTruck.Name = "lbTypeOfTruck";
            this.lbTypeOfTruck.Size = new System.Drawing.Size(105, 17);
            this.lbTypeOfTruck.TabIndex = 5;
            this.lbTypeOfTruck.Text = "Tipo de camión";
            // 
            // cbTypeOfTruck
            // 
            this.cbTypeOfTruck.FormattingEnabled = true;
            this.cbTypeOfTruck.Location = new System.Drawing.Point(173, 57);
            this.cbTypeOfTruck.Margin = new System.Windows.Forms.Padding(4);
            this.cbTypeOfTruck.Name = "cbTypeOfTruck";
            this.cbTypeOfTruck.Size = new System.Drawing.Size(208, 24);
            this.cbTypeOfTruck.TabIndex = 4;
            // 
            // tbNumberOfAxles
            // 
            this.tbNumberOfAxles.Location = new System.Drawing.Point(173, 25);
            this.tbNumberOfAxles.Margin = new System.Windows.Forms.Padding(4);
            this.tbNumberOfAxles.Name = "tbNumberOfAxles";
            this.tbNumberOfAxles.Size = new System.Drawing.Size(208, 22);
            this.tbNumberOfAxles.TabIndex = 1;
            // 
            // lbNumberOfAxles
            // 
            this.lbNumberOfAxles.AutoSize = true;
            this.lbNumberOfAxles.Location = new System.Drawing.Point(23, 28);
            this.lbNumberOfAxles.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbNumberOfAxles.Name = "lbNumberOfAxles";
            this.lbNumberOfAxles.Size = new System.Drawing.Size(108, 17);
            this.lbNumberOfAxles.TabIndex = 0;
            this.lbNumberOfAxles.Text = "Número de ejes";
            // 
            // gbBus
            // 
            this.gbBus.Controls.Add(this.lbTypeOfService);
            this.gbBus.Controls.Add(this.cbTypeOfService);
            this.gbBus.Controls.Add(this.tbSeatingCapacity);
            this.gbBus.Controls.Add(this.lbSeatingCapacity);
            this.gbBus.Location = new System.Drawing.Point(455, 132);
            this.gbBus.Margin = new System.Windows.Forms.Padding(4);
            this.gbBus.Name = "gbBus";
            this.gbBus.Padding = new System.Windows.Forms.Padding(4);
            this.gbBus.Size = new System.Drawing.Size(407, 98);
            this.gbBus.TabIndex = 15;
            this.gbBus.TabStop = false;
            this.gbBus.Text = "Autobus";
            // 
            // lbTypeOfService
            // 
            this.lbTypeOfService.AutoSize = true;
            this.lbTypeOfService.Location = new System.Drawing.Point(23, 60);
            this.lbTypeOfService.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTypeOfService.Name = "lbTypeOfService";
            this.lbTypeOfService.Size = new System.Drawing.Size(108, 17);
            this.lbTypeOfService.TabIndex = 4;
            this.lbTypeOfService.Text = "Tipo de servicio";
            // 
            // cbTypeOfService
            // 
            this.cbTypeOfService.FormattingEnabled = true;
            this.cbTypeOfService.Location = new System.Drawing.Point(173, 57);
            this.cbTypeOfService.Margin = new System.Windows.Forms.Padding(4);
            this.cbTypeOfService.Name = "cbTypeOfService";
            this.cbTypeOfService.Size = new System.Drawing.Size(208, 24);
            this.cbTypeOfService.TabIndex = 3;
            // 
            // tbSeatingCapacity
            // 
            this.tbSeatingCapacity.Location = new System.Drawing.Point(173, 25);
            this.tbSeatingCapacity.Margin = new System.Windows.Forms.Padding(4);
            this.tbSeatingCapacity.Name = "tbSeatingCapacity";
            this.tbSeatingCapacity.Size = new System.Drawing.Size(208, 22);
            this.tbSeatingCapacity.TabIndex = 1;
            // 
            // lbSeatingCapacity
            // 
            this.lbSeatingCapacity.AutoSize = true;
            this.lbSeatingCapacity.Location = new System.Drawing.Point(23, 28);
            this.lbSeatingCapacity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbSeatingCapacity.Name = "lbSeatingCapacity";
            this.lbSeatingCapacity.Size = new System.Drawing.Size(141, 17);
            this.lbSeatingCapacity.TabIndex = 0;
            this.lbSeatingCapacity.Text = "Cantidad de asientos";
            // 
            // gbCar
            // 
            this.gbCar.Controls.Add(this.lbTypeOfAutomobile);
            this.gbCar.Controls.Add(this.cbTypeOfAutomobile);
            this.gbCar.Controls.Add(this.tbNumberOfDoors);
            this.gbCar.Controls.Add(this.lbNumberOfDoors);
            this.gbCar.Location = new System.Drawing.Point(455, 26);
            this.gbCar.Margin = new System.Windows.Forms.Padding(4);
            this.gbCar.Name = "gbCar";
            this.gbCar.Padding = new System.Windows.Forms.Padding(4);
            this.gbCar.Size = new System.Drawing.Size(407, 98);
            this.gbCar.TabIndex = 14;
            this.gbCar.TabStop = false;
            this.gbCar.Text = "Automóvil";
            // 
            // lbTypeOfAutomobile
            // 
            this.lbTypeOfAutomobile.AutoSize = true;
            this.lbTypeOfAutomobile.Location = new System.Drawing.Point(23, 60);
            this.lbTypeOfAutomobile.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTypeOfAutomobile.Name = "lbTypeOfAutomobile";
            this.lbTypeOfAutomobile.Size = new System.Drawing.Size(120, 17);
            this.lbTypeOfAutomobile.TabIndex = 3;
            this.lbTypeOfAutomobile.Text = "Tipo de automóvil";
            // 
            // cbTypeOfAutomobile
            // 
            this.cbTypeOfAutomobile.FormattingEnabled = true;
            this.cbTypeOfAutomobile.Location = new System.Drawing.Point(173, 57);
            this.cbTypeOfAutomobile.Margin = new System.Windows.Forms.Padding(4);
            this.cbTypeOfAutomobile.Name = "cbTypeOfAutomobile";
            this.cbTypeOfAutomobile.Size = new System.Drawing.Size(208, 24);
            this.cbTypeOfAutomobile.TabIndex = 2;
            // 
            // tbNumberOfDoors
            // 
            this.tbNumberOfDoors.Location = new System.Drawing.Point(173, 25);
            this.tbNumberOfDoors.Margin = new System.Windows.Forms.Padding(4);
            this.tbNumberOfDoors.Name = "tbNumberOfDoors";
            this.tbNumberOfDoors.Size = new System.Drawing.Size(208, 22);
            this.tbNumberOfDoors.TabIndex = 1;
            // 
            // lbNumberOfDoors
            // 
            this.lbNumberOfDoors.AutoSize = true;
            this.lbNumberOfDoors.Location = new System.Drawing.Point(23, 28);
            this.lbNumberOfDoors.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbNumberOfDoors.Name = "lbNumberOfDoors";
            this.lbNumberOfDoors.Size = new System.Drawing.Size(130, 17);
            this.lbNumberOfDoors.TabIndex = 0;
            this.lbNumberOfDoors.Text = "Número de puertas";
            // 
            // cbColorOfVehicle
            // 
            this.cbColorOfVehicle.FormattingEnabled = true;
            this.cbColorOfVehicle.Location = new System.Drawing.Point(160, 165);
            this.cbColorOfVehicle.Margin = new System.Windows.Forms.Padding(4);
            this.cbColorOfVehicle.Name = "cbColorOfVehicle";
            this.cbColorOfVehicle.Size = new System.Drawing.Size(265, 24);
            this.cbColorOfVehicle.TabIndex = 9;
            // 
            // cbBrandOfVehicle
            // 
            this.cbBrandOfVehicle.FormattingEnabled = true;
            this.cbBrandOfVehicle.Location = new System.Drawing.Point(160, 132);
            this.cbBrandOfVehicle.Margin = new System.Windows.Forms.Padding(4);
            this.cbBrandOfVehicle.Name = "cbBrandOfVehicle";
            this.cbBrandOfVehicle.Size = new System.Drawing.Size(265, 24);
            this.cbBrandOfVehicle.TabIndex = 7;
            // 
            // tbPassengerCapacity
            // 
            this.tbPassengerCapacity.Location = new System.Drawing.Point(160, 198);
            this.tbPassengerCapacity.Margin = new System.Windows.Forms.Padding(4);
            this.tbPassengerCapacity.Name = "tbPassengerCapacity";
            this.tbPassengerCapacity.Size = new System.Drawing.Size(265, 22);
            this.tbPassengerCapacity.TabIndex = 11;
            // 
            // lbFiscalValue
            // 
            this.lbFiscalValue.AutoSize = true;
            this.lbFiscalValue.Location = new System.Drawing.Point(25, 234);
            this.lbFiscalValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbFiscalValue.Name = "lbFiscalValue";
            this.lbFiscalValue.Size = new System.Drawing.Size(81, 17);
            this.lbFiscalValue.TabIndex = 12;
            this.lbFiscalValue.Text = "Valor Fiscal";
            // 
            // lbPassengerCapacity
            // 
            this.lbPassengerCapacity.AutoSize = true;
            this.lbPassengerCapacity.Location = new System.Drawing.Point(25, 202);
            this.lbPassengerCapacity.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPassengerCapacity.Name = "lbPassengerCapacity";
            this.lbPassengerCapacity.Size = new System.Drawing.Size(75, 17);
            this.lbPassengerCapacity.TabIndex = 10;
            this.lbPassengerCapacity.Text = "Capacidad";
            // 
            // lbColor
            // 
            this.lbColor.AutoSize = true;
            this.lbColor.Location = new System.Drawing.Point(25, 169);
            this.lbColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbColor.Name = "lbColor";
            this.lbColor.Size = new System.Drawing.Size(41, 17);
            this.lbColor.TabIndex = 8;
            this.lbColor.Text = "Color";
            // 
            // lbBrand
            // 
            this.lbBrand.AutoSize = true;
            this.lbBrand.Location = new System.Drawing.Point(25, 135);
            this.lbBrand.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbBrand.Name = "lbBrand";
            this.lbBrand.Size = new System.Drawing.Size(47, 17);
            this.lbBrand.TabIndex = 6;
            this.lbBrand.Text = "Marca";
            // 
            // lbModel
            // 
            this.lbModel.AutoSize = true;
            this.lbModel.Location = new System.Drawing.Point(25, 103);
            this.lbModel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbModel.Name = "lbModel";
            this.lbModel.Size = new System.Drawing.Size(83, 17);
            this.lbModel.TabIndex = 4;
            this.lbModel.Text = "Año modelo";
            // 
            // lbRegistration
            // 
            this.lbRegistration.AutoSize = true;
            this.lbRegistration.Location = new System.Drawing.Point(25, 71);
            this.lbRegistration.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbRegistration.Name = "lbRegistration";
            this.lbRegistration.Size = new System.Drawing.Size(43, 17);
            this.lbRegistration.TabIndex = 2;
            this.lbRegistration.Text = "Placa";
            // 
            // lbTypeOfVehicle
            // 
            this.lbTypeOfVehicle.AutoSize = true;
            this.lbTypeOfVehicle.Location = new System.Drawing.Point(25, 38);
            this.lbTypeOfVehicle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTypeOfVehicle.Name = "lbTypeOfVehicle";
            this.lbTypeOfVehicle.Size = new System.Drawing.Size(114, 17);
            this.lbTypeOfVehicle.TabIndex = 0;
            this.lbTypeOfVehicle.Text = "Tipo de Vehículo";
            // 
            // tbModel
            // 
            this.tbModel.Location = new System.Drawing.Point(160, 100);
            this.tbModel.Margin = new System.Windows.Forms.Padding(4);
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(265, 22);
            this.tbModel.TabIndex = 5;
            // 
            // tbRegistration
            // 
            this.tbRegistration.Location = new System.Drawing.Point(160, 68);
            this.tbRegistration.Margin = new System.Windows.Forms.Padding(4);
            this.tbRegistration.Name = "tbRegistration";
            this.tbRegistration.Size = new System.Drawing.Size(265, 22);
            this.tbRegistration.TabIndex = 3;
            // 
            // tbFiscalValue
            // 
            this.tbFiscalValue.Location = new System.Drawing.Point(160, 230);
            this.tbFiscalValue.Margin = new System.Windows.Forms.Padding(4);
            this.tbFiscalValue.Name = "tbFiscalValue";
            this.tbFiscalValue.Size = new System.Drawing.Size(265, 22);
            this.tbFiscalValue.TabIndex = 13;
            // 
            // cbTypeOfVehicle
            // 
            this.cbTypeOfVehicle.FormattingEnabled = true;
            this.cbTypeOfVehicle.Location = new System.Drawing.Point(160, 34);
            this.cbTypeOfVehicle.Margin = new System.Windows.Forms.Padding(4);
            this.cbTypeOfVehicle.Name = "cbTypeOfVehicle";
            this.cbTypeOfVehicle.Size = new System.Drawing.Size(265, 24);
            this.cbTypeOfVehicle.TabIndex = 1;
            // 
            // gbResult
            // 
            this.gbResult.Controls.Add(this.txtMandatoryInsurance);
            this.gbResult.Controls.Add(this.lbMandatoryInsurance);
            this.gbResult.Location = new System.Drawing.Point(16, 443);
            this.gbResult.Margin = new System.Windows.Forms.Padding(4);
            this.gbResult.Name = "gbResult";
            this.gbResult.Padding = new System.Windows.Forms.Padding(4);
            this.gbResult.Size = new System.Drawing.Size(891, 124);
            this.gbResult.TabIndex = 19;
            this.gbResult.TabStop = false;
            this.gbResult.Text = "Resultado";
            // 
            // txtMandatoryInsurance
            // 
            this.txtMandatoryInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMandatoryInsurance.Location = new System.Drawing.Point(455, 41);
            this.txtMandatoryInsurance.Margin = new System.Windows.Forms.Padding(4);
            this.txtMandatoryInsurance.Name = "txtMandatoryInsurance";
            this.txtMandatoryInsurance.ReadOnly = true;
            this.txtMandatoryInsurance.Size = new System.Drawing.Size(399, 41);
            this.txtMandatoryInsurance.TabIndex = 3;
            // 
            // lbMandatoryInsurance
            // 
            this.lbMandatoryInsurance.AutoSize = true;
            this.lbMandatoryInsurance.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMandatoryInsurance.Location = new System.Drawing.Point(213, 48);
            this.lbMandatoryInsurance.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbMandatoryInsurance.Name = "lbMandatoryInsurance";
            this.lbMandatoryInsurance.Size = new System.Drawing.Size(199, 31);
            this.lbMandatoryInsurance.TabIndex = 2;
            this.lbMandatoryInsurance.Text = "Seguro a pagar";
            // 
            // MandatoryInsuranceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 582);
            this.Controls.Add(this.gbResult);
            this.Controls.Add(this.gbVehicle);
            this.Controls.Add(this.panelHeader);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MandatoryInsuranceForm";
            this.Text = "Calculadora de Seguro Obligatorio";
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            this.gbVehicle.ResumeLayout(false);
            this.gbVehicle.PerformLayout();
            this.gbTruck.ResumeLayout(false);
            this.gbTruck.PerformLayout();
            this.gbBus.ResumeLayout(false);
            this.gbBus.PerformLayout();
            this.gbCar.ResumeLayout(false);
            this.gbCar.PerformLayout();
            this.gbResult.ResumeLayout(false);
            this.gbResult.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbFormTitle;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.GroupBox gbVehicle;
        private System.Windows.Forms.TextBox tbPassengerCapacity;
        private System.Windows.Forms.Label lbFiscalValue;
        private System.Windows.Forms.Label lbPassengerCapacity;
        private System.Windows.Forms.Label lbColor;
        private System.Windows.Forms.Label lbBrand;
        private System.Windows.Forms.Label lbModel;
        private System.Windows.Forms.Label lbRegistration;
        private System.Windows.Forms.Label lbTypeOfVehicle;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.TextBox tbRegistration;
        private System.Windows.Forms.TextBox tbFiscalValue;
        private System.Windows.Forms.ComboBox cbTypeOfVehicle;
        private System.Windows.Forms.ComboBox cbColorOfVehicle;
        private System.Windows.Forms.ComboBox cbBrandOfVehicle;
        private System.Windows.Forms.GroupBox gbTruck;
        private System.Windows.Forms.TextBox tbNumberOfAxles;
        private System.Windows.Forms.Label lbNumberOfAxles;
        private System.Windows.Forms.GroupBox gbBus;
        private System.Windows.Forms.TextBox tbSeatingCapacity;
        private System.Windows.Forms.Label lbSeatingCapacity;
        private System.Windows.Forms.GroupBox gbCar;
        private System.Windows.Forms.TextBox tbNumberOfDoors;
        private System.Windows.Forms.Label lbNumberOfDoors;
        private System.Windows.Forms.Button btClearForm;
        private System.Windows.Forms.Button btCalculateMandatoryInsurance;
        private System.Windows.Forms.GroupBox gbResult;
        private System.Windows.Forms.TextBox txtMandatoryInsurance;
        private System.Windows.Forms.Label lbMandatoryInsurance;
        private System.Windows.Forms.ComboBox cbTypeOfAutomobile;
        private System.Windows.Forms.ComboBox cbTypeOfTruck;
        private System.Windows.Forms.ComboBox cbTypeOfService;
        private System.Windows.Forms.Label lbTypeOfTruck;
        private System.Windows.Forms.Label lbTypeOfService;
        private System.Windows.Forms.Label lbTypeOfAutomobile;
    }
}