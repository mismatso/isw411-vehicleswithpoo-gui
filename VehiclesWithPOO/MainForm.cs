﻿using System;
using System.Windows.Forms;

using VehiclesWithPOO.Views;

namespace VehiclesWithPOO
{
    public partial class MainForm : Form
    {
        MandatoryInsuranceForm mandatoryInsuranceForm;

        public MainForm()
        {
            InitializeComponent();
        }

        private void tsmiExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsmiCalculateMandatoryInsurance_Click(object sender, EventArgs e)
        {
            try
            {
                if (mandatoryInsuranceForm.IsDisposed == true)
                    mandatoryInsuranceForm = new MandatoryInsuranceForm();
                else
                    mandatoryInsuranceForm.Activate();
            }
            catch (Exception) { mandatoryInsuranceForm = new MandatoryInsuranceForm(); }

            mandatoryInsuranceForm.MdiParent = this;
            mandatoryInsuranceForm.ShowInTaskbar = false;
            mandatoryInsuranceForm.StartPosition = FormStartPosition.CenterScreen;
            mandatoryInsuranceForm.Show();
        }
    }
}
